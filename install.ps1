Set-ExecutionPolicy -ExecutionPolicy Bypass -Force
Write-Host "`n`nThis script will fix the Dell 5540 realtek driver issue" -Foregroundcolor Green -BackgroundColor Black
[void](Read-Host "`n`nPress ENTER to start . . . or CTRL+C to exit")
# Install Part 1 [Installing Realtek drivers]
$install=Start-process -FilePath "$PSScriptRoot\realtek\RealtekAudio\Setup.exe" -ArgumentList '/s','/v','/qn' -PassThru
$install
for($i = 0; $i -le 100; $i = ($i + 1) % 100)
{
    Write-Progress -Activity "[F5 IT] Dell 5540 audio fix Part 1 of 2" -PercentComplete $i -Status "Installing Realtek drivers for 5540 issue. Please Wait . . ."
    Start-Sleep -Milliseconds 100
    if ($install.HasExited) {
        Write-Progress -Activity "Installer (F5 IT)" -Completed
        break
    }
}
if ($install.ExitCode -eq 0)
{
    Write-Host "`n`n Part 1 successfully installed, Please wait for second part to complete" -Foregroundcolor Green
}
else
{
    Write-Host "Something Failed on Part 1. Exit code: $($install.ExitCode)"
}
# Install Part 2 [This will uninstall and reinstall driver which seems to fix the issue]
$install2=Start-process -FilePath "$PSScriptRoot\realtek\RealtekAudio\Setup.exe" -ArgumentList '/s','/v','/qn' -PassThru
$install2
for($i = 0; $i -le 100; $i = ($i + 1) % 100)
{
    Write-Progress -Activity "[F5 IT] Dell 5540 audio fix Part 2 of 2" -PercentComplete $i -Status "Installing Realtek drivers for 5540 issue. Please Wait . . ."
    Start-Sleep -Milliseconds 100
    if ($install2.HasExited) {
        Write-Progress -Activity "Installer (F5 IT)" -Completed
        break
    }
}
if ($install2.ExitCode -eq 0)
{
    Write-Host "`n`n Part 2 successfully installed, Please restart machine now..." -Foregroundcolor Green
    [void](Read-Host "`n`nPress ENTER to Continue")
}
else
{
    Write-Host "Something Failed on Part 2. Exit code: $($install2.ExitCode)"
    [void](Read-Host "`n`nPress ENTER to Continue")
}