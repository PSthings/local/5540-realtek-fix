# Author: Sunny Kahlon
# Updated: 12/14/2020
# Description: This scripts downloads, extracts and installs realtek audio fix

# Bypassing ExecutionPolicy for this $PID
Set-ExecutionPolicy -ExecutionPolicy Bypass -Force
# Define File and Paths
$Url = 'https://gitlab.com/PSthings/local/5540-realtek-fix/-/raw/master/realtek.zip' 
$ZipFile = "$env:userprofile\Downloads\" + $(Split-Path -Path $Url -Leaf) 
$Destination= "$env:userprofile\Downloads\"
$foldername='realtek'

# Hide Progress Bar as its known to cause slow download speeds
$progressPreference = 'silentlyContinue'
# Download package from $Url
Write-Host "`nDownloading package to $Destination. Please wait . . ." -ForegroundColor Green
Invoke-WebRequest -Uri $Url -OutFile $ZipFile -ErrorAction Stop
# Revert the progressPreference back to Continue
$progressPreference = 'Continue'
# Unzip downloaded .zip to ~\Downloads\ using Expand-Drive
Write-Host "`nExtracting package ..." -ForegroundColor Yellow
Expand-Archive -path "$ZipFile" -destinationpath "$destination\$foldername"
# unblock install.ps1 to temporarily bypass security warning message
Unblock-File "$Destination\$foldername\install.ps1"
#Starts install.ps1 in new admin powershell window and closes this window
Start-Process powershell "-nologo -File $Destination\$foldername\install.ps1"
stop-process -Id $PID